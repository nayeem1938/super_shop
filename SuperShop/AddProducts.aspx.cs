﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace SuperShop
{
    public partial class AddProducts : System.Web.UI.Page
    {
        DataHandelar dh = new DataHandelar();
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            string path = Server.MapPath("./ProductImage/");
            if (FileUpload1.HasFile)
            {
                string ext = Path.GetExtension(FileUpload1.FileName);
                if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")
                {
                    FileUpload1.SaveAs(path + TextBox1.Text+ ".jpg");
                        string name = "./ProductImage/" + TextBox1.Text+".jpg";
                    string query = @"insert into products
                            values('" + TextBox1.Text + "', '" + TextBox2.Text + "', '" + TextBox3.Text + "', '" + TextBox4.Text +
                            "','" + name + "')";
                    if (dh.exexuteQuery(query) == 1)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Product Added Successfully');</script>", false);

                    }

                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Upload Failed');</script>", false);

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Please Upload jpg, jpeg or png File Only');</script>", false);

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Please Upload a Photo');</script>", false);
            }
        }
    }
}