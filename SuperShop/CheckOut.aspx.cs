﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace SuperShop
{
    public partial class CheckOut : System.Web.UI.Page
    {
        DataHandelar dh = new DataHandelar();
        String connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr;
        int sr = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            loader();
        }
        public void loader()
        {

            if (!IsPostBack)
            {
                dt.Columns.Add("sno");
                dt.Columns.Add("productid");
                dt.Columns.Add("productname");
                dt.Columns.Add("quantity");
                dt.Columns.Add("price");
                dt.Columns.Add("totalprice");
                if (Request.QueryString["id"] != null)
                {
                    if (Session["Buyitems"] == null)
                    {
                        sr = 1;
                        cartBase(sr);
                    }
                    else
                    {
                        dt = (DataTable)Session["buyitems"];
                        sr = dt.Rows.Count;
                        cartBase(sr + 1);
                    }
                }
                else
                {
                    dt = (DataTable)Session["buyitems"];
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    if (GridView1.Rows.Count > 0)
                    {
                        GridView1.FooterRow.Cells[4].Text = "Total Amount";
                        GridView1.FooterRow.Cells[5].Text = grandtotal().ToString();

                    }


                }
                Label1.Text = GridView1.Rows.Count.ToString();

            }
            Label2.Text = DateTime.Now.ToShortDateString();
            findOrderId();
        }

        public void cartBase(int sr)
        {
            SqlConnection con = new SqlConnection(connection);
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();

            dr = dt.NewRow();
            String myquery = "select * from products where product_id=" + Request.QueryString["id"];
            cmd.CommandText = myquery;
            cmd.Connection = con;
            da.SelectCommand = cmd;
            da.Fill(ds);
            dr["sno"] = sr;
            dr["productid"] = ds.Tables[0].Rows[0]["product_id"].ToString();
            dr["productname"] = ds.Tables[0].Rows[0]["product_name"].ToString();
            dr["quantity"] = Request.QueryString["quantity"];
            dr["price"] = ds.Tables[0].Rows[0]["price"].ToString();
            int price = Convert.ToInt16(ds.Tables[0].Rows[0]["price"].ToString());
            int quantity = Convert.ToInt16(Request.QueryString["quantity"].ToString());
            int totalprice = price * quantity;
            dr["totalprice"] = totalprice;
            dt.Rows.Add(dr);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            Session["buyitems"] = dt;
            GridView1.FooterRow.Cells[4].Text = "Total Amount";
            GridView1.FooterRow.Cells[5].Text = grandtotal().ToString();
            Response.Redirect("AddToCart.aspx");

        }
        public int grandtotal()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["buyitems"];
            int nrow = dt.Rows.Count;
            int i = 0;
            int gtotal = 0;
            while (i < nrow)
            {
                gtotal = gtotal + Convert.ToInt32(dt.Rows[i]["totalprice"].ToString());

                i = i + 1;
            }
            return gtotal;
        }
        public void findOrderId()
        {
            String orderid;
            orderid = "Order" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString();

            Label1.Text = orderid;
        }
        public string findLineId()
        {
            String lineid;
            lineid = "line"+ sr + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString();
            return lineid;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Session["customer_phone"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                
                dt = (DataTable)Session["buyitems"];
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    String query = @"insert into order_line(line_id,product_id,quantity,line_total, order_id)
                    values('" + findLineId() + "', " + dt.Rows[i]["productid"] + "," + dt.Rows[i]["quantity"] + ",'" + dt.Rows[i]["totalprice"] + "','" + Label1.Text + "')";
                    dh.exexuteQuery(query);
                }
                String querys = @"insert into orders
                    values('" + Label1.Text + "', '" + Label2.Text + "','" + grandtotal()+ "','" + Session["customer_phone"] + "')";
                dh.exexuteQuery(querys);
                Response.Redirect("SuccessFullOrder.aspx");
            }
        }
    }
}