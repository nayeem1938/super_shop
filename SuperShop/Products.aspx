﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="SuperShop.Products1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">

     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You Have
    <asp:Label ID="lbl_no" runat="server" Font-Bold="True" style="text-align: left" ForeColor="#FF3300"></asp:Label>
&nbsp;Products in Your Cart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/AddToCart.aspx">Go To Cart</asp:HyperLink>
    <br />
    <asp:DataList ID="DataList1" runat="server" DataKeyField="product_id" DataSourceID="SqlDataSource2" RepeatColumns="4" style="text-align: center" OnItemCommand="DataList1_ItemCommand">
        <ItemTemplate>
            <table style="width: 30%; height: 124px;">
                <tr>
                    <td>
                        <asp:Image ID="Image1" runat="server" Height="116px" ImageUrl='<%# Eval("image") %>' Width="229px" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Product ID" style="text-align: left"></asp:Label>
                        <asp:Label runat="server" Text='<%# Eval("product_id") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 24px">
                        <asp:Label ID="Label8" runat="server" Text="Name"></asp:Label>
                        <asp:Label ID="Label2" runat="server" style="text-align: center" Text='<%# Eval("product_name") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="Price"></asp:Label>
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("price") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label7" runat="server" Text="Stock"></asp:Label>
                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("stock") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="Quantity"></asp:Label>
                        <asp:DropDownList ID="DropDownList1" runat="server">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;<asp:Label ID="Label10" runat="server"></asp:Label>
                        <asp:ImageButton ID="ImageButton1" runat="server" Height="26px" Width="174px" ImageUrl="~/css/images/download (2).jpg" CommandArgument='<%# Eval("product_id")%>' CommandName="AddToCart" />
                    </td>
                </tr>
            </table>
            <br />
        </ItemTemplate>
</asp:DataList>
<asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:SuperShopConnectionString2 %>" SelectCommand="SELECT [product_id], [product_name], [price], [stock], [image] FROM [products] ORDER BY [product_name], [price]"></asp:SqlDataSource>
</asp:Content>
