﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace SuperShop
{
    public partial class AddToCart : System.Web.UI.Page
    {
        DataHandelar dh = new DataHandelar();
        String connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr;
        protected void Page_Load(object sender, EventArgs e)
        {
            loader();
        }
        public void loader()
        {
            
            if (!IsPostBack)
            {
                dt.Columns.Add("sno");
                dt.Columns.Add("productid");
                dt.Columns.Add("productname");
                dt.Columns.Add("quantity");
                dt.Columns.Add("price");
                dt.Columns.Add("totalprice");
                if (Request.QueryString["id"] != null)
                {
                    if (Session["Buyitems"] == null)
                    {
                        int sr = 1;
                        cartBase(sr);
                    }
                    else
                    {
                        dt = (DataTable)Session["buyitems"];
                        int sr = dt.Rows.Count;
                        cartBase(sr+1);
                    }
                }
                else
                {
                    dt = (DataTable)Session["buyitems"];
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    if (GridView1.Rows.Count > 0)
                    {
                        GridView1.FooterRow.Cells[4].Text = "Total Amount";
                        GridView1.FooterRow.Cells[5].Text = grandtotal().ToString();

                    }


                }
                Label1.Text = GridView1.Rows.Count.ToString();

            }
        }

        public void cartBase(int sr)
        {
            SqlConnection con = new SqlConnection(connection);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();

            dr = dt.NewRow();
            String myquery = "select * from products where product_id=" + Request.QueryString["id"];
            cmd.CommandText = myquery;
            cmd.Connection = con;
            da.SelectCommand = cmd;
            da.Fill(ds);
            dr["sno"] = sr;
            dr["productid"] = ds.Tables[0].Rows[0]["product_id"].ToString();
            dr["productname"] = ds.Tables[0].Rows[0]["product_name"].ToString();
            dr["quantity"] = Request.QueryString["quantity"];
            dr["price"] = ds.Tables[0].Rows[0]["price"].ToString();
            int price = Convert.ToInt16(ds.Tables[0].Rows[0]["price"].ToString());
            int quantity = Convert.ToInt16(Request.QueryString["quantity"].ToString());
            int totalprice = price * quantity;
            dr["totalprice"] = totalprice;
            dt.Rows.Add(dr);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            Session["buyitems"] = dt;
            GridView1.FooterRow.Cells[4].Text = "Total Amount";
            GridView1.FooterRow.Cells[5].Text = grandtotal().ToString();
            Response.Redirect("AddToCart.aspx");

        }
        public int grandtotal()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["buyitems"];
            int nrow = dt.Rows.Count;
            int i = 0;
            int gtotal = 0;
            while (i < nrow)
            {
                gtotal = gtotal + Convert.ToInt32(dt.Rows[i]["totalprice"].ToString());

                i = i + 1;
            }
            return gtotal;
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            dt = (DataTable)Session["buyitems"];
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                int sr;
                int sr1;
                string qdata;
                string dtdata;
                sr = Convert.ToInt32(dt.Rows[i]["sno"].ToString());
                TableCell cell = GridView1.Rows[e.RowIndex].Cells[0];
                qdata = cell.Text;
                dtdata = sr.ToString();
                sr1 = Convert.ToInt32(qdata);
                if (sr == sr1)
                {
                    dt.Rows[i].Delete();
                    dt.AcceptChanges();
                    break;
                }
            }
            for (int i = 1; i <= dt.Rows.Count; i++)
            {
                dt.Rows[i - 1]["sno"] = i;
                dt.AcceptChanges();
            }
            Session["buyitems"] = dt;
            Response.Redirect("AddToCart.aspx");
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            Response.Redirect("ModifyOrder.aspx?sno=" + GridView1.SelectedRow.Cells[0].Text);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("CheckOut.aspx");
        }
    }
}