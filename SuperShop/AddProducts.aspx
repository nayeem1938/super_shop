﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddProducts.aspx.cs" Inherits="SuperShop.AddProducts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 61px;
        }
        .auto-style2 {
            width: 97px;
        }
        .auto-style3 {
            width: 103px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="body" runat="server">
    <div class="product_list">
        <table class="tbl_add">
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label1" runat="server" Text="Product Name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
                <td></td>
                <td class="auto-style1">
                    <asp:Label ID="Label2" runat="server" Text="Price"></asp:Label>
                </td>
                <td class="auto-style3">
                    <asp:TextBox ID="TextBox2" runat="server" Height="17px" Width="121px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label3" runat="server" Text="Reorder"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </td>
                <td></td>
                <td class="auto-style1">
                    <asp:Label ID="Label4" runat="server" Text="Stock"></asp:Label>
                </td>
                <td class="auto-style3">
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label5" runat="server" Text="Image"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" />  
                </td>
                <td></td>
                <td class="auto-style1">
                    <asp:Label ID="Label6" runat="server" Text="Photo"></asp:Label>
                </td>
                <td class="auto-style3">
                    &nbsp;</td>
             </tr>
            <tr>
                <td class="auto-style2"></td>
                <td></td>
                <td></td>
                <td class="auto-style1"></td>
                <td class="auto-style3">
                    <asp:Button ID="Button1"  runat="server" Text="Add Product" OnClick="Button1_Click" Width="123px" />
                </td>
            </tr>

        </table>
    </div>


</asp:Content>
